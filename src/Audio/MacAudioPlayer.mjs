import util from 'util';
import { exec } from 'child_process';
import Debug from 'debug';

const debug = Debug('HapticTilesExtensions:MacAudioPlayer');

const execAsync = util.promisify(exec);

export const languageToVoiceMap = {
	'cs-cz': 'Zuzana',
	'en-us': 'Alex',
};

export default class MacAudioPlayer {

	constructor(speak, defaultLanguage) {
		this.speak = speak;
		this.defaultVoice = languageToVoiceMap[defaultLanguage];
	}

	async play(input) {
		debug('play input', input);
		if (typeof input === 'string') {
			return this.playSpeak(input);
		} else if (input?.type === 'voice') {
			return this.playSpeak(input.value, input?.language);
		} else if (input?.type === 'sound') {
			return this.playPlay(input.value);
		} else {
			//throw "Unknown play input type";
		}
	}

	async playSpeak(text, language) {
		const voice = languageToVoiceMap[language] ?? this.defaultVoice;
		await this.speak(text, voice);
	}

	async playPlay(path) {
		await execAsync(`afplay ${path}`);
	}

}
