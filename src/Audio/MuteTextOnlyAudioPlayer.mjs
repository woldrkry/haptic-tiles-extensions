import fs from 'fs-extra';
import Debug from 'debug';

const debug = Debug('HapticTilesExtensions:MuteTextOnlyAudioPlayer');

export default class MuteTextOnlyAudioPlayer {

	constructor(speak) {
		debug('Speak function supplied', typeof speak !== 'undefined');
		this.speak = speak;
		if (!this.speak) {
			throw new Error('Speak function not supplied');
		}
	}

	play(input) {
		if (typeof input === 'string') {
			return this.playSpeak(input);
		} else if (input.type === 'voice') {
			return this.playSpeak(input.value);
		} else if (input.type === 'sound') {
			return this.playPlay(input.value);
		} else {
			throw "Unknown play input type";
		}
	}

	async playSpeak(text) {
		debug('Speak:	', text);
	}

	async playPlay(path) {
		if (await fs.pathExists(path)) {
			debug('Play:	', path);
		} else {
			throw new Error(`File not found: ${path}`);
		}
	}

}
