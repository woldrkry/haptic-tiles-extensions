import { stdin } from 'process'
import Debug from 'debug';

const debug = Debug('HapticTilesExtensions:KeyboardBrailleModule');

export default class KeyboardBrailleModule {

	size = 6;

	constructor() {}

	async init() {
		if (stdin.isTTY) {
			stdin.setRawMode(true);
		}
		stdin.resume()
		stdin.setEncoding('utf8')

		stdin.on('data', (key) => this._onKeydown(key))
	}

	on(event, callback) {
		if (event === 'keydown') {
			this.keydownCallback = callback;
		}
	}

	_onKeydown(key) {
		debug('keyboard key', key);
		let braille;
		switch (key) {
			case '\u0003':
				// CTRL-C
				process.exit()
			case 'j':
				braille = '4';
				break;

			case 'k':
				braille = '5';
				break;

			case 'l':
				braille = '6';
				break;

			case 'f':
				braille = '1';
				break;

			case 'd':
				braille = '2';
				break;

			case 's':
				braille = '3';
				break;

			default:
				break;
		}

		this.keydownCallback({
			symbol: 0,
			braille,
		});
	}

}
