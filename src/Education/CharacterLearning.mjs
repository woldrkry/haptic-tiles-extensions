import fs from 'fs-extra';
import Debug from 'debug'
import prompts from 'prompts';
import debounce from '../Utils/debounce.mjs';

const debug = Debug('HapticTilesExtensions:CharacterLearning');

export default class CharacterLearning {

	constructor(module, speak, dataPath) {
		this.module = module;
		this.speak = speak;
		this.dataPath = dataPath;
	}

	async start() {
		this.stop();
		this.czech_braille = await fs.readJSON(this.dataPath);
		this.selectedKey = await this._selectCharacter();
		const symbols = this._convertToBraille(this.selectedKey);
		this.history = this._initHistory(symbols);
		this.module.on(
			'keydown',
			debounce(this._onModuleKeyDown.bind(this)),
		);
	}

	stop() {
		this._clearHistory();
		this.module.on('keydown', () => { /* do nothing */ });
	}

	async _onModuleKeyDown(data) {
		debug('_onModuleKeyDown', data)
		const braille = data.braille;
		if (braille) {
			const symbolHistory = this.history?.[data.symbol]
			if (symbolHistory && symbolHistory.pointsHistory[data.braille] !== undefined) {
				symbolHistory.pointsHistory[data.braille] = true
				console.log('Said: ', braille);
				await this.speak(braille);
			}

			if (!symbolHistory ||
				symbolHistory && !symbolHistory.pointsHistory[data.braille]) {
				console.log('Wrong braille: ', braille);
				await this.speak('Špatně!');
			}

			debug('Current history before check: ', this.history)
			const symbolsCount = this.history.length;
			const completedSymbols = this.history.filter(symbolHistory => {
				const pointsHistory = symbolHistory.pointsHistory
				const points = Object.keys(pointsHistory)
				const pointsCount = points.length
				const pointsCompleted = points.filter((point) => pointsHistory[point])
				const pointsCompletedCount = pointsCompleted.length

				if (pointsCompletedCount === pointsCount) {
					return true;
				}
			});

			if (completedSymbols.length === symbolsCount) {
				await this.speak('Správně!')
				await this.speak('Vybrali jste všechny body!')
				await this.speak('Znak se skládá z: ')
				for(const char of this.czech_braille[this.selectedKey]) {
					if (char === '.') {
						await this.speak('a')
					} else {
						await this.speak(char)
					}
				}
				await this.speak('Konec cvičení!')
				this.start();
			}
		}
		debug('Current history: ', this.history)
	}

	async _selectCharacter() {
		this.speak('Vyberte znak ke cvičení: ');
		const response = await prompts({
			type: 'autocompleteMultiselect',
			name: 'value',
			message: 'Vyberte znak ke cviceni',
			choices: Object.keys(this.czech_braille).map(char => ({
				title: char,
				value: char,
			})),
			max: 1,
			hint: '- Nahoru a dolu prochazeni. Mezerou zmenit vyber. Enter pro potvrzeni.',
			instructions: false,
		});

		const selectedKey = response.value?.[0];

		if (!selectedKey) {
			this.speak('Nevybrali jste žádný znak ke cvičení. Končím.');
			throw new Error('No character selected');
		}

		this.speak('Vybrali jste znak: ' + selectedKey)
		return selectedKey;
	}

	_convertToBraille(char) {
		const symbols = this.czech_braille[char].split('.')
		debug('Expected symbols: ', symbols)
		return symbols;
	}

	_initHistory(symbols) {
		const history = []
		for (const symbol of symbols) {
			const symbolHistory = {
				pointsHistory: {},
			}
			for (const point of symbol) {
				symbolHistory.pointsHistory[point] = false
			}

			history.push(symbolHistory);
		}
		debug('Initial history: ', history)
		return history;
	}

	_clearHistory() {
		this.history = [];
	}

}
