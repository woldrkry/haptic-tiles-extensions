import Debug from 'debug';

const debug = Debug('HapticTilesExtensions:TalkingSignSystemRemote');

const brailleToRemoteButtonMap = {
	"1": "1",
	"2": "3",
	"3": "5",
	"4": "2",
	"5": "4",
	"6": "6",
}

export default class TalkingSignSystemRemote {

	constructor(
		brailleModule,
	) {
		this.brailleModule = brailleModule;
		this.brailleModule.on('keydown', (...args) => this._onBrailleModuleKeydown(...args));
	}

	on(event, callback) {
		if (event === 'keydown') {
			debug('on', event);
			this.keydownCallback = callback;
		}
	}

	_onBrailleModuleKeydown({braille}) {
		debug('_onBrailleModuleKeydown', braille);
		const remoteButton = brailleToRemoteButtonMap[braille];
		this.keydownCallback?.(remoteButton);
	}

}
